# Compile and Install Golden Cheetah for Fedora 23

    sudo dnf install gcc-c++ qt-devel qtwebkit-devel qt5-linguist 
    git clone git://github.com/GoldenCheetah/GoldenCheetah.git
    cd GoldenCheetah
    cp gcconfig.pri src/gcconfig.pri
    cp qwt/qwtconfig.pri.in qwt/qwtconfig.pri
    qmake-qt4 build.pro
    make
    sudo cp GoldenCheetah /usr/bin/
    sudo cp GoldenCheetah.desktop /usr/share/applications/
    sudo cp GoldenCheetah.png /usr/share/icons/
    
# Build Information

    Version:    (Developer Build)
    DB Schema:  124
    Metrics:    275
    OS:         Linux
    QT:         4.8.7
    QWT:        6.1.1
    GCC:        5.1.1
    SRMIO:      none
    OAUTH:      yes
    D2XX:       none
    QWTPLOT3D:  none
    KML:        none
    ICAL:       none
    USBEXPRESS: none
    LIBUSB:     none
    Wahoo API:  none
    VLC:        none
    VIDEO:      none
    SAMPLERATE: none
    SSL:        yes